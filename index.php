﻿<?php 
	include("bd/bd.php");

	$SQLT = mysqli_query($con, "SELECT count(*) as total FROM tbpalavras");
	$total = mysqli_fetch_assoc($SQLT);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>JP</title>
	<link rel="stylesheet" href="../style.css">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript" ></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
</head>
<body>
	<header class="red">
		<nav>
			<i class="fa fa-bars fa-2x" aria-hidden="true"></i>
			<ul>
				<li><a href="/restrito">Área restrita</a></li>
				<li><a href="frases.php">Frases prontas</a></li>
				<li><a class="ativo" href="index.php">Palavras</a></li>
			</ul>
		</nav>
	</header>
	<section>		

		<h1 class="red">Lista de <?php echo $total['total']; ?> palavras</h1>
		<form>
			<input type="text" name="BuscarPalavra" placeholder="Buscar:" id="BuscarPalavra">
		</form><br>
		<table id="busca">
			<tr>
				<th>Português</th>
				<th>Romaji</th>
				<th>Katakana/Hiragana</th>
				<th class="hidden">Detalhes</th>
				</tr>
			<?php 
				$SQL = mysqli_query($con, "SELECT * FROM tbpalavras order by portugues asc");
				while($result = mysqli_fetch_assoc($SQL)){	
					echo "<tr><td class=\"hidden\">".$result['portugues']."</td>";
					echo "<td class=\"show\"><a href=\"?id=".$result['id']."\"><strong>".$result['portugues']."</strong></a></td>";
					echo "<td>".$result['romaji']."</td>";
					echo "<td>".$result['japones']."</td>";
					echo "<td class=\"hidden\"><a href=\"detalhes.php?id=".$result['id']."\"<i class=\"fa fa-arrow-right\" aria-hidden=\"true\"></i></a></td>";
					}
			?>
		</table>
	</section>
	<footer class="red">
		<p>Desenvolvido por: Alana O. Rodrigues. 2016.</p>
	</footer>
		<script type="text/javascript" language="javascript">
	    $(document).ready(function() {
	        $('#BuscarPalavra').keyup(function() {
	            $.ajax({
	                type: 'POST',
	                url: 'BuscarPalavra.php',
	                async: true,
	                data: { conteudo: " like '%"+$('#BuscarPalavra').val()+"%'"},
	                success: function(result) {
	            		$('#busca').html(result);
	                }
	           });
			});  
		}); 
		</script>
</body>
</html>