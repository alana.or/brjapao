<?php 
	include("bd/bd.php");

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>JP - Detalhes</title>
	<link rel="stylesheet" href="../style.css">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript" ></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
</head>
<body>	
	<header class="red">
		<nav>
			<i class="fa fa-bars fa-2x" aria-hidden="true"></i>
			<ul>
				<li><a href="/restrito">Área restrita</a></li>
				<li><a href="frases.php">Frases prontas</a></li>
				<li><a class="ativo" href="index.php">Palavras</a></li>
			</ul>
		</nav>
	</header>
	<section>		
		<h1 class="red">Detalhes</h1>
		<div class="detalhes">
			<?php 
				$SQL = mysqli_query($con, "SELECT * FROM tbpalavras where id=".$_GET['id']);
				$palavra = mysqli_fetch_assoc($SQL);
				echo "<div class=\"kanji\">".$palavra['kanji']."</div>";
				echo "<div class=\"portugues\">PT: ".$palavra['portugues']."</div>";
				echo "<div class=\"japones\">JP: ".$palavra['japones']."</div>";
				echo "<div class=\"descricao\">Descrição: ".$palavra['descricao']."</div>";
			?>			
		</div>
		<div class="detalhes">
			<h2>Frases:</h2><br>
			<?php 
				$SQLF = mysqli_query($con, "SELECT * FROM tbpalavras, tbfrases, tbfrasespalavras where tbpalavras.id = tbfrasespalavras.idpalavras and tbfrases.id = tbfrasespalavras.idfrases and tbpalavras.id=".$_GET['id']);

				$frases = mysqli_fetch_assoc($SQLF);
				echo "<div class=\"frases\"><p>".$frases['japones']."</p><p>".$frases['romaji']."</p><p>".$frases['portugues']."</p></div>";
			?>	
		</div>
	</section>
	<footer class="red">
		<p>Desenvolvido por: Alana O. Rodrigues. 2016.</p>
	</footer>
</body>
</html>