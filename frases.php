<?php 
	include("bd/bd.php");

	$SQLT = mysqli_query($con, "SELECT count(*) as total FROM tbfrases");
	$total = mysqli_fetch_assoc($SQLT);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>JP</title>
	<link rel="stylesheet" href="../style.css">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript" ></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
</head>
<body>	
	<header class="red">
		<nav>
			<i class="fa fa-bars fa-2x" aria-hidden="true"></i>
			<ul>
				<li><a href="/restrito">Área restrita</a></li>
				<li><a class="ativo" href="frases.php">Frases prontas</a></li>
				<li><a href="index.php">Palavras</a></li>
			</ul>
		</nav>
	</header>
	<section>		
		<h1 class="red">Lista de <?php echo $total['total']; ?> frases</h1>
		<form>
			<input type="text" name="BuscarFrase" placeholder="Buscar:" id="BuscarFrase">
		</form><br>
		<table id="busca">
			<tr>
				<th>Português</th>
				<th>Romaji</th>
				<th>Katakana/Hiragana</th>
			</tr>
			<?php 
				$SQL = mysqli_query($con, "SELECT * FROM tbfrases order by portugues asc");
				while($result = mysqli_fetch_assoc($SQL)){	
					echo "<tr><td>".$result['portugues']."</td>";
					echo "<td>".$result['romaji']."</td>";
					echo "<td>".$result['japones']."</td></tr>";
				}
			?>
		</table>
	</section>
	<footer class="red">
		<p>Desenvolvido por: Alana O. Rodrigues. 2016.</p>
	</footer>
		<script type="text/javascript" language="javascript">
	    $(document).ready(function() {
	        $('#BuscarFrase').keyup(function() {
	            $.ajax({
	                type: 'POST',
	                url: 'BuscarFrasePronta.php',
	                async: true,
	                data: { conteudo: " like '%"+$('#BuscarFrase').val()+"%'"},
	                success: function(result) {
	            		$('#busca').html(result);
	                }
	           });
			});  
		}); 
		</script>
</body>
</html>