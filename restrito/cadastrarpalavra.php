<?php 
	include("../bd/bd.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>JP - Área restrita - Cadastro de novas palavras</title>
	<link rel="stylesheet" href="../style.css">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript" ></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
</head>
<body>
	<header class="red">
		<nav>
			<i class="fa fa-bars fa-2x" aria-hidden="true"></i>
			<ul>
				<li><a href="sair.php">Sair</a></li>
				<li><a href="frases.php">Frases</a></li>
				<li><a class="ativo" href="palavras.php">Palavras</a></li>
			</ul>
		</nav>
	</header>
	<section>		
		
		<h1 class="red">Cadastrar nova palavra</h1>
		<div class="alerta"><i class="fa fa-exclamation" aria-hidden="true"></i>  &nbsp;Atenção: Verifique se a palavra já está no sistema.</div>
		<form action="cadastrar.php" method="post">
			<input type="text" name="portugues" id="portugues" required="required" placeholder="Palavra em português">
			<span id="busca"></span>
			<input type="text" name="japones" required="required" placeholder="Palavra em hiragana/katakana">
			<input type="text" name="romaji" required="required" placeholder="Palavra em romaji">
			<input type="text" name="kanji" placeholder="Palavra em Kanji">
			<textarea name="descricao" id="descricao" cols="30" rows="10" placeholder="Descrição"></textarea>
			<input class="red" type="submit" value="Cadastrar">
		</form>
	</section>
	<footer class="red">
		<p>Desenvolvido por: Alana O. Rodrigues. 2016.</p>
	</footer>
		<script type="text/javascript" language="javascript">
	    $(document).ready(function() {
	        $('#portugues').keyup(function() {
	            $.ajax({
	                type: 'POST',
	                url: 'Buscarportugues.php',
	                async: true,
	                data: { conteudo: " like '%"+$('#portugues').val()+"%'"},
	                success: function(result) {
	            		$('#busca').html(result);
	                }
	           });
			});  
		}); 
		</script>
</body>
</html>