<?php 
	include("../bd/bd.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>JP - Área restrita - Cadastro de novas frases</title>
	<link rel="stylesheet" href="../style.css">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript" ></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
</head>
<body>
	<header class="red">
		<nav>
			<i class="fa fa-bars fa-2x" aria-hidden="true"></i>
			<ul>
				<li><a href="sair.php">Sair</a></li>
				<li><a class="ativo" href="frases.php">Frases</a></li>
				<li><a href="palavras.php">Palavras</a></li>
			</ul>
		</nav>
	</header>
	<section>		
	<?php if ($_GET['cad']): ?>
		<div class="cad"><i class="fa fa-check" aria-hidden="true"></i>  &nbsp;Frase cadastrada com sucesso!</div>
	<?php endif ?>
		
		<h1 class="red">Cadastrar nova frase</h1>
		<div class="alerta"><i class="fa fa-exclamation" aria-hidden="true"></i>  &nbsp;Atenção: Verifique se a frase já está no sistema.</div>
		<form action="cadastrar.php" method="post">
			<input type="text" name="portugues" id="portugues" autocomplete="off" required="required" placeholder="Frase em português">
			<span id="busca"></span>
			<input type="text" name="japones" required="required" autocomplete="off" placeholder="Frase em hiragana/katakana">
			<input type="text" name="romaji" required="required" autocomplete="off" placeholder="Frase em romaji">
			<input class="red" type="submit" value="Cadastrar">
			<div id="vinculo">
				<span class="palavra"></span>
			</div>
		</form>
	</section>
	<footer class="red">
		<p>Desenvolvido por: Alana O. Rodrigues. 2016.</p>
	</footer>
		<script type="text/javascript" language="javascript">
	    $(document).ready(function() {

	        $('#portugues').keyup(function() {
        		$.ajax({
	                type: 'POST',
	                url: 'Vinculo.php',
	                async: true,
	                data: { conteudo: $('#portugues').val()},
	                success: function(result) {
	            		$('#vinculo').html(result);
	                }
	           	});

	            $.ajax({
	                type: 'POST',
	                url: 'BuscarFrase.php',
	                async: true,
	                data: { conteudo: " like '%"+$('#portugues').val()+"%'"},
	                success: function(result) {
	            		$('#busca').html(result);
	                }
	           });
			});  
		}); 
		</script>
</body>
</html>