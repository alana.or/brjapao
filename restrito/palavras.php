<?php 
	include("../bd/bd.php");

	$SQLT = mysqli_query($con, "SELECT count(*) as total FROM tbpalavras");
	$total = mysqli_fetch_assoc($SQLT);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>JP - Área restrita - Lista de palavras</title>
	<link rel="stylesheet" href="../style.css">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript" ></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
</head>
<body>
	<header class="red">
		<nav>
			<i class="fa fa-bars fa-2x" aria-hidden="true"></i>
			<ul>
				<li><a href="sair.php">Sair</a></li>
				<li><a href="frases.php">Frases</a></li>
				<li><a class="ativo" href="palavras.php">Palavras</a></li>
			</ul>
		</nav>
	</header>
	<section>		
		
	<?php if ($_GET['cad']): ?>
		<div class="cad"><i class="fa fa-check" aria-hidden="true"></i>  &nbsp;Palavra cadastrada com sucesso!</div>
	<?php endif ?>
	<?php if ($_GET['e']): ?>
		<div class="cad"><i class="fa fa-check" aria-hidden="true"></i>  &nbsp;Palavra excluida com sucesso!</div>
	<?php endif ?>
	<?php if ($_GET['ex']): ?>
		<div class="cad"><i class="fa fa-check" aria-hidden="true"></i>  &nbsp;Deseja realmente excluir <?php echo $_GET['expl'] ?> ? <a href="excluir.php?id=<?php echo $_GET['ex']; ?>"><br>Sim</a>&nbsp;&nbsp;-&nbsp;&nbsp;<a href="palavras.php">Não</a></div>
	<?php endif ?>
		<a class="novafrase red" href="cadastrarpalavra.php">Adicionar palavra</a>
		<h1 class="red">Lista de <?php echo $total['total']; ?> palavras</h1>
		<form>
			<input type="text" name="BuscarPalavra" placeholder="Buscar:" id="BuscarPalavra">
		</form><br>
		<table id="busca">
			<tr> 
				<th>Português</th>
				<th>Romaji</th><th>&nbsp;</th>
				<th class="hidden">Editar</th>
				<th class="hidden">Excluir</th>
			</tr>
			<?php 
				$SQL = mysqli_query($con, "SELECT * FROM tbpalavras order by portugues asc");
				while($result = mysqli_fetch_assoc($SQL)){	
					echo "<tr><td class=\"hidden\">".$result['portugues']."</td>";
					echo "<td class=\"show\"><a href=\"?id=".$result['id']."\"><strong>".$result['portugues']."</strong></a></td>";
					echo "<td>".$result['romaji']."</td><td>&nbsp;</td>";
					echo "<td class=\"hidden\"><a href=\"?id=".$result['id']."\"<i class=\"fa fa-arrow-right\" aria-hidden=\"true\"></i></a></td>";
					echo "<td class=\"hidden\"><a href=\"palavras.php?expl=".$result['portugues']."&ex=".$result['id']."\"<i class=\"fa fa-arrow-right\" aria-hidden=\"true\"></i></a></td></tr>";
				}
			?>
		</table>
	</section>
	<footer class="red">
		<p>Desenvolvido por: Alana O. Rodrigues. 2016.</p>
	</footer>
		<script type="text/javascript" language="javascript">
	    $(document).ready(function() {
	        $('#BuscarPalavra').keyup(function() {
	            $.ajax({
	                type: 'POST',
	                url: 'BuscarPalavraEx.php',
	                async: true,
	                data: { conteudo: " like '%"+$('#BuscarPalavra').val()+"%'"},
	                success: function(result) {
	            		$('#busca').html(result);
	                }
	           });
			});  
		}); 
		</script>
</body>
</html>